Edit firebase.json file, update your hosting key by defining rewrite rules:

```json
"hosting": {
    "public": "dist",
    "rewrites": [ {
      "source": "**",
      "destination": "/index.html"
    } ]
  }
```