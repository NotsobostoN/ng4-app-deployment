## Update ng4-app-deployment on bitbucket repo
```bash
#log in to c9.io and choose project ng4-app
cd ~/workspace/deployment
rm public/*
cp ~/workspace/ng4-app/dist/* public/.
git add public/*
git commit -m "change comment"
git push -u origin master
#enter password when prompted
```
