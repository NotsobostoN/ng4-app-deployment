## My ng4-app README
----

```bash
npm -version (3.10.8)
node --version (v6.9.1)
npm uninstall --save-data @angular/cli
npm cache clean -f
npm install -g @angular/cli@latest
ng help
ng version (@angular/cli: 1.1.2)
#
#Note: If you see “Invalid Host header” when running Angular/cli development server c9.io, then do the following:
#
#"Edit the following line in node_modules/webpack-dev-server/lib/Server.js (line 425):
#
#change to
#
#return true;
#
#In cloud9 IDE then run: ng serve --port 8080 --host 0.0.0.0
#Open browser and navigate to https://my-ng4-app-notsoboston.c9users.io/
#
#For more details, see this: https://github.com/angular/angular-cli/issues/6070
#
```

[[Create ng4-app-deployment repo from scratch]](fromScratch.md)

[[Update ng4-app-deployment repo with app changes]](update.md)

[[Bring down ng4-app FIRST TIME into tofu on c9.io]](bringDownAppFirstTime.md)

[[Bring down ng4-app into tofu on c9.io]](bringDownApp.md)

[[Deploy ng4-app from c9.io to firebase hosting]](deploy.md)

[[Angular app with Route giving 404 when deployed to Firebase hosting]](firebaseAngularRoute404.md)

[[Watch - Getting started with firebase hosting]](https://www.youtube.com/watch?v=meofoNuK3vo)

[[Read - Getting started with Angular's Router]](https://toddmotto.com/angular-component-router)

[[Read - Managing state in Angular Apps]](https://blog.nrwl.io/managing-state-in-angular-applications-22b75ef5625f)

[[Link to this app]](https://tofu.wluz.com/)

[[Link to source]](https://github.com/iyengark/ng4-app)