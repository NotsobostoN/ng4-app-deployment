## Create ng4-app-deployment repo from scratch (contents of ~/workspace/ng4-app/dist/*)
```bash
#create an empty project called ng4-app-deployment on bitbucket repo
#log in to c9.io and choose project ng4-app 
cd ~/workspace
git clone https://NotsobostoN@bitbucket.org/NotsobostoN/ng4-app-deployment.git
cd ng4-app-deployment
echo "# My ng4-app project's README" >> README.md
git add README.md
mkdir public
git add public/
cp ~/workspace/ng4-app/dist/* public/.
git add public/*
git commit -m "Initial commit"
git push -u origin master
#enter password when prompted
```