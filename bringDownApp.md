## Bring down ng4-app deployment into tofu's sandbox from bitbucket repo
```bash
# log in to c9.io, open project tofu and start a Terminal window
cd ~/workspce/sandbox/ng4-app-deployment
git pull
rm ~/workspace/tofu/public/*
cp ~/workspace/sandbox/ng4-app-deployment/public/* ~/workspace/tofu/public/.
```
